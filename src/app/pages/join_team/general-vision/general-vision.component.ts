import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

declare let $: any;

@Component({
  selector: 'app-general-vision',
  templateUrl: './general-vision.component.html',
  styleUrls: ['./general-vision.component.css']
})
export class GeneralVisionComponent implements OnInit {

  constructor(private translate: TranslateService) { }

  ngOnInit(): void {
    this.loadLang();
  }

  async loadLang() {
    this.translate.setDefaultLang("es");
    this.translate.use("es");
    const translations = await this.translate.get(['who_we_are']).toPromise().then(trans => trans);
  }
}
