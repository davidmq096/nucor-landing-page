import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(private translate: TranslateService, private titleService: Title) { }

  ngOnInit(): void {
    this.loadLang();
    this.titleService.setTitle("Nucor JFE");
  }

  async loadLang() {
    this.translate.setDefaultLang("es");
    this.translate.use("es");
    const translations = await this.translate.get(['index']).toPromise().then(trans => trans);
  }
}
