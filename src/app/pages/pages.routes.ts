import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { OverviewComponent } from './who_we_are/overview/overview.component';
import { OurCultureComponent } from './who_we_are/our-culture/our-culture.component';
import { PhilosophyComponent } from './who_we_are/philosophy/philosophy.component';
import { LocationComponent } from './who_we_are/location/location.component';
import { WWDOverviewComponent } from './what_we_do/overview/overview.component';
import { OurProductsComponent } from './what_we_do/our-products/our-products.component';
import { GeneralVisionComponent } from './join_team/general-vision/general-vision.component';

const pagesRoutes: Routes = [
    { path: 'inicio', component: LandingComponent, data: { title: 'Nucor' } },
    { path: 'quienes_somos/nuestra_historia', component: OverviewComponent, data: { title: 'Nuestra historia' } },
    { path: 'quienes_somos/nuestra_cultura', component: OurCultureComponent, data: { title: 'Nuestra cultura' } },
    { path: 'quienes_somos/ubicacion', component: LocationComponent, data: { title: 'Ubicacion' } },
    { path: 'quienes_somos/filosofia', component: PhilosophyComponent, data: { title: 'Filosofia' } },
    { path: 'que_hacemos/vision_general', component: WWDOverviewComponent, data: { title: 'Vision general' } },
    { path: 'que_hacemos/nuestros_productos', component: OurProductsComponent, data: { title: 'Nuestros productos' } },
    { path: 'unete_equipo/vision_general', component: GeneralVisionComponent, data: { title: 'Vision general' } },
    { path: '', redirectTo: '/inicio', pathMatch: 'full', data: { title: 'Nucor' } }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
