import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-wwdoverview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class WWDOverviewComponent implements OnInit {

  constructor(private translate: TranslateService, private titleService: Title) { }

  ngOnInit(): void {
    this.loadLang();
    this.titleService.setTitle("Visión general");
  }

  async loadLang() {
    this.translate.setDefaultLang("es");
    this.translate.use("es");
    const translations = await this.translate.get(['who_we_are']).toPromise().then(trans => trans);
  }
}
