import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-our-products',
  templateUrl: './our-products.component.html',
  styleUrls: ['./our-products.component.css']
})
export class OurProductsComponent implements OnInit {

  constructor(private translate: TranslateService, private titleService: Title) { }

  ngOnInit(): void {
    this.loadLang();
    this.titleService.setTitle("Productos");
  }

  async loadLang() {
    this.translate.setDefaultLang("es");
    this.translate.use("es");
    const translations = await this.translate.get(['who_we_are']).toPromise().then(trans => trans);
  }
}
