import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LandingComponent } from './landing/landing.component';
import { PAGES_ROUTES } from './pages.routes';
import { TranslateModule } from '@ngx-translate/core';
import { OverviewComponent } from './who_we_are/overview/overview.component';
import { OurCultureComponent } from './who_we_are/our-culture/our-culture.component';
import { PhilosophyComponent } from './who_we_are/philosophy/philosophy.component';
import { WWDOverviewComponent } from './what_we_do/overview/overview.component';
import { OurProductsComponent } from './what_we_do/our-products/our-products.component';
import { GeneralVisionComponent } from './join_team/general-vision/general-vision.component';
import { LocationComponent } from './who_we_are/location/location.component';

@NgModule({
  declarations: [
    LandingComponent,
    OverviewComponent,
    OurCultureComponent,
    PhilosophyComponent,
    WWDOverviewComponent,
    OurProductsComponent,
    GeneralVisionComponent,
    LocationComponent
    
  ],
  imports: [
    CommonModule,
    PAGES_ROUTES,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule
  ]
})
export class PagesModule { }
