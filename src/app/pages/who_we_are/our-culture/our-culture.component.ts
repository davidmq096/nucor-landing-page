import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-our-culture',
  templateUrl: './our-culture.component.html',
  styleUrls: ['./our-culture.component.css']
})
export class OurCultureComponent implements OnInit {

  constructor(private translate: TranslateService, private titleService: Title) { }

  ngOnInit(): void {
    this.loadLang();
    this.titleService.setTitle("Nuestra cultura");
  }

  async loadLang() {
    this.translate.setDefaultLang("es");
    this.translate.use("es");
    const translations = await this.translate.get(['who_we_are']).toPromise().then(trans => trans);
  }

}
