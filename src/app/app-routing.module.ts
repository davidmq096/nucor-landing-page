import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent} from './pages/pages.component';
import { PagesModule} from './pages/pages.module';

const routes: Routes = [
  { path: '', component: PagesComponent, loadChildren: () => PagesModule },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
