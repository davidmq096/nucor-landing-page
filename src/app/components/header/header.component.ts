import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private translate: TranslateService, private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle("Nucor JFE");
  }

  changeLanguage (lang: string) {
    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
  }

}
